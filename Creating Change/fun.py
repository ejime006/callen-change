def single(digit):
    return {
        '0': '',
        '1': 'One',
        '2': 'Two',
        '3': 'Three',
        '4': 'Four',
        '5': 'Five',
        '6': 'Six',
        '7': 'Seven',
        '8': 'Eight',
        '9': 'Nine',
    }[digit]


def teens(digit):
    return {
        '10': 'Ten',
        '11': 'Eleven',
        '12': 'Twelve',
        '13': 'Thirteen',
        '14': 'Fourteen',
        '15': 'Fifteen',
        '16': 'Sixteen',
        '17': 'Seventeen',
        '18': 'Eighteen',
        '19': 'Nineteen',
    }[digit]


def xten(digit):
    return {
        '0': '',
        '1': '',
        '2': 'Twenty',
        '3': 'Thirty',
        '4': 'Fourty',
        '5': 'Fifty',
        '6': 'Sixty',
        '7': 'Seventy',
        '8': 'Eighty',
        '9': 'Ninety',
    }[digit]


def hundred(digit, string):
    if digit != '0':
        string.append(single(digit))
        string.append('hundred')
    return string


def two(number, string):
    if int(number) >= 10 and int(number) < 20:
        string.append(teens(number))

    elif int(number) >= 20:
        string.append(xten(number[0]))
        string.append(single(number[1]))

    else:
        string.append(single(number[1]))
    return string


def three(num0, num1, num2, string):
    if num0 == 0 and num1 == 0 and num2 == 0:
        return string
    if num0 == 0 and num1 == 0:
        string.append(single(num2))
    elif num0 == 0:
        string.append(two(num1 + num2))
    else:
        string.append(hundred(num0, string))
        string.append(two(num1 + num2, string))

    return string


def cleanOutput(string):
    temp = ''
    for i in string:
        if type(i) == str:
            temp = "{} {}".format(temp, i)
    return temp[1:]


def pop3(number):
    number.pop(0)
    number.pop(0)
    number.pop(0)
    return number


def threePlaces(number):
    while (len(number) % 3) != 0:
        number.insert(0, '0')
    return number


def change_words(number, string):
    length = len(number)
    if length == 15:
        string.append(three(number[0], number[1], number[2], string))
        string.append('trillion')
        number = pop3(number)
        change_words(number, string)

    if length == 12:
        string.append(three(number[0], number[1], number[2], string))
        string.append('billion')
        number = pop3(number)
        change_words(number, string)

    if length == 9:
        string.append(three(number[0], number[1], number[2], string))
        string.append('million')
        number = pop3(number)
        change_words(number, string)

    if length == 6:
        string.append(three(number[0], number[1], number[2], string))
        string.append('thousand')
        number = pop3(number)
        change_words(number, string)

    if length == 3:
        string.append(three(number[0], number[1], number[2], string))
    return string


'''



# trillion billion million thousand hundreds. cents
      3       3        3       3       3
'''
