def billCoins(change):
    string = []
    string1 = []
    ret = []

    change = change * 100

    hundy = 10000
    fifty = 5000
    twenty = 2000
    ten = 1000
    five = 500
    one = 100
    quarter = 25
    dime = 10
    nickle = 5
    penny = 1

    hundy_total = 0
    fifty_total = 0
    twenty_total = 0
    ten_total = 0
    five_total = 0
    one_total = 0
    quarter_total = 0
    dime_total = 0
    nickle_total = 0
    penny_total = 0

    hundy_total = int(change // hundy)
    change = change - hundy_total * hundy
    fifty_total = int(change // fifty)
    change = change - fifty_total * fifty
    twenty_total = int(change // twenty)
    change = change - twenty_total * twenty
    ten_total = int(change // ten)
    change = change - ten_total * ten
    five_total = int(change // five)
    change = change - five_total * five
    one_total = int(change // one)
    change = change - one_total * one
    quarter_total = int(change // quarter)
    change = change - quarter_total * quarter
    dime_total = int(change // dime)
    change = change - dime_total * dime
    nickle_total = int(change // nickle)
    change = change - nickle_total * nickle
    penny_total = int(change // penny)
    change = change - penny_total * penny

    string = "100s: {}\n50s: {}\n20s: {}\n10s: {}\n5s: {}\n1s: {}".format(
        hundy_total, fifty_total, twenty_total, ten_total, five_total, one_total).split('\n')
    string1 = "Quarters: {}\nDines: {}\nNickles: {}\nPennys: {}".format(
        quarter_total, dime_total, nickle_total, penny_total).split('\n')

    for i in string:
        if i[-1] != '0':
            ret.append(i)
    for i in string1:
        if i[-1] != '0':
            ret.append(i)
    return ret
